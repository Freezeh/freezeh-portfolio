# Freezeh Portfolio

[![style: styled-components](https://img.shields.io/badge/style-%F0%9F%92%85%20styled--components-orange.svg?colorB=daa357&colorA=db748e)](https://github.com/styled-components/styled-components) [![Netlify Status](https://api.netlify.com/api/v1/badges/712da066-bf49-4455-9dd7-80b19325302f/deploy-status)](https://app.netlify.com/sites/trusting-mahavira-1e6863/deploys) [![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff.svg)](https://lerna.js.org/)

## Getting started

### Development

Run: `yarn dev`

### Deployment

Build: `docker-compose build web`

Run: `docker-compose up web`
