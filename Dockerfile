FROM node:11-alpine

WORKDIR /usr/src/app

# Install lerna globally
RUN npm i lerna -g --loglevel notice

# Copy the modules that are needed
# COPY modules/core ./modules/core

# Copying source files
COPY packages/web ./src/web
COPY package.json .
COPY yarn.lock .
COPY lerna.json .

# Installing dependencies
RUN lerna bootstrap

# Running the app
CMD yarn --cwd src/web run dev