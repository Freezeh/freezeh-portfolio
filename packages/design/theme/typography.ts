import {
  FontFamilyProperty,
  FontSizeProperty,
  FontWeightProperty,
  LetterSpacingProperty,
  LineHeightProperty,
} from 'csstype'

const BASE_FONT_SIZE = 16
const SCALE_FACTOR = 1.25
const LINE_HEIGHT_FACTOR = 1.3

const FONT_URL =
  'https://fonts.googleapis.com/css?family=Amatic+SC|Open+Sans:400,600&display=swap'
const PRIMARY_FONT_FAMILY = `'Open Sans', sans-serif`
const SECONDARY_FONT_FAMILY = `'Amatic SC', cursive`

export interface TypographyEntry {
  fontFamily: FontFamilyProperty
  fontSize: FontSizeProperty<string>
  fontWeight: FontWeightProperty
  lineHeight: LineHeightProperty<string>
  letterSpacing: LetterSpacingProperty<string>
}

export interface TypographyInterface {
  display: TypographyEntry
  heading: TypographyEntry
  subheading: TypographyEntry
  navigation: TypographyEntry
  bodyText: TypographyEntry
  caption: TypographyEntry
  button: TypographyEntry
  link: TypographyEntry
  fontUrl: string
}

const calculateSizing = (entry: number) => {
  const fontSize =
    entry === 0
      ? BASE_FONT_SIZE
      : BASE_FONT_SIZE * Math.pow(SCALE_FACTOR, entry)
  const lineHeight = fontSize * LINE_HEIGHT_FACTOR

  return {
    fontSize: `${fontSize}px`,
    lineHeight: `${lineHeight}px`,
  }
}

const createEntry = (
  entry: number,
  fontFamily: FontFamilyProperty,
  fontWeight: FontWeightProperty,
  letterSpacing: LetterSpacingProperty<string>,
) => {
  return {
    ...calculateSizing(entry),
    fontFamily,
    fontWeight,
    letterSpacing,
  }
}

export const typography: TypographyInterface = {
  display: createEntry(3, SECONDARY_FONT_FAMILY, 'bold', '0.5px'),
  heading: createEntry(2, SECONDARY_FONT_FAMILY, 'normal', '0.5px'),
  subheading: createEntry(1, PRIMARY_FONT_FAMILY, 'normal', '0.4px'),
  navigation: createEntry(3, SECONDARY_FONT_FAMILY, 'bold', '0.7px'),
  bodyText: createEntry(0, PRIMARY_FONT_FAMILY, 'normal', '0.2px'),
  caption: createEntry(-1, PRIMARY_FONT_FAMILY, 'normal', '0'),
  button: createEntry(0, PRIMARY_FONT_FAMILY, 'bold', '0.5px'),
  link: createEntry(1, PRIMARY_FONT_FAMILY, 'normal', '0.4px'),
  fontUrl: FONT_URL,
}
