import { ColorProperty } from 'csstype'

/**
 *
 * @param colour_1 <ColorProperty> first colour to mix like #FFCC00
 * @param colour_2 <ColorProperty> first colour to mix like #CCFF00
 * @param weight <number> qty from 0 to 100 for the mix. Default is 50
 */
const mix = (
  colour_1: ColorProperty,
  colour_2: ColorProperty,
  weight: number = 50,
) => {
  function d2h(d: any) {
    return d.toString(16)
  }
  function h2d(h: any) {
    return parseInt(h, 16)
  }

  let color = '#'

  for (let i = 1; i <= 6; i += 2) {
    const v1 = h2d(colour_1.substr(i, 2))
    const v2 = h2d(colour_2.substr(i, 2))

    let val = d2h(Math.floor(v2 + (v1 - v2) * (weight / 100.0)))

    while (val.length < 2) {
      val = `0${val}`
    }

    color += val
  }

  return color
}

/**
 *
 * @param colour <ColorProperty> fThe colour you want to tint
 * @param weight <number> qty from 0 to 100 for the tint. Default is 50
 */
const tint = (colour: ColorProperty, amount: number = 50) => {
  return mix('#FFFFFF', colour, Math.abs(amount))
}

/**
 *
 * @param colour <ColorProperty> fThe colour you want to tint
 * @param weight <number> qty from 0 to 100 for the shade. Default is 50
 */
const shade = (colour: ColorProperty, amount: number = 50) => {
  return mix('#000000', colour, Math.abs(amount))
}

export { tint, shade, mix }
