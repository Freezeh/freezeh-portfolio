import { palette, ColorPeletteInterface } from './colors'
import { typography, TypographyInterface } from './typography'
import { mq } from './breakpoints'

import * as styledComponents from 'styled-components'

export interface ThemeInterface {
  palette: ColorPeletteInterface
  typography: TypographyInterface
  mq: string[]
  siteWidth: string
}

const theme: ThemeInterface = {
  palette,
  typography,
  mq,
  siteWidth: '1400px',
}

const {
  default: styled,
  css,
  createGlobalStyle,
  keyframes,
  ThemeProvider,
  withTheme,
} = styledComponents as styledComponents.ThemedStyledComponentsModule<
  ThemeInterface
>

export {
  theme,
  styled,
  css,
  createGlobalStyle,
  keyframes,
  ThemeProvider,
  withTheme,
}
