import { ColorProperty } from 'csstype'
import { tint, shade } from './mix'

export interface PaletteEntryInterface {
  lightest: ColorProperty
  lighter: ColorProperty
  light: ColorProperty
  base: ColorProperty
  dark: ColorProperty
  darker: ColorProperty
}

export interface ColorPeletteInterface {
  brand: PaletteEntryInterface
  accent: PaletteEntryInterface
  greys: PaletteEntryInterface
}

const brand: ColorProperty = '#605BFF'
const accent: ColorProperty = '#FFC107'
const black: ColorProperty = '#000000'

export const palette: ColorPeletteInterface = {
  brand: {
    lightest: tint(brand, 90),
    lighter: tint(brand, 60),
    light: tint(brand, 20),
    base: brand,
    dark: shade(brand, 20),
    darker: shade(brand, 40),
  },
  accent: {
    lightest: tint(accent, 90),
    lighter: tint(accent, 60),
    light: tint(accent, 20),
    base: accent,
    dark: shade(accent, 20),
    darker: shade(accent, 40),
  },
  greys: {
    lightest: tint(black, 95),
    lighter: tint(black, 80),
    light: tint(black, 60),
    base: black,
    dark: tint(black, 35),
    darker: tint(black, 15),
  },
}
