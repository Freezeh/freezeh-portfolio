const breakpoints = [600, 900, 1200]

export const mq: string[] = breakpoints.map(bp => `@media (min-width: ${bp}px)`)
