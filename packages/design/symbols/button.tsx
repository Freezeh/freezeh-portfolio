import * as React from 'react'
import styled from 'styled-components'

interface ButtonProps extends React.HTMLAttributes<HTMLButtonElement> {
  emphasis: 'primary' | 'secondary'
}

export const Button = (props: ButtonProps) => {
  const StyledButton = styled.button<{ emphasis: 'primary' | 'secondary' }>`
    ${props => props.theme.typography.button};
    padding: 8px 32px;
    border-radius: 4px;

    ${props => {
      if (props.emphasis === 'primary') {
        return `
          background-color: ${props.theme.palette.accent.base};
          border: 1px solid ${props.theme.palette.accent.base};
          color: ${props.theme.palette.greys.darker};

          &:hover, &:focus {
            background-color: ${props.theme.palette.accent.light};
          }

          &:active {
            background-color: ${props.theme.palette.accent.dark};
          }
        `
      }
      return `
          background-color: ${props.theme.palette.brand.base};
          border: 1px solid ${props.theme.palette.brand.base};
          color: ${props.theme.palette.greys.lightest};

          &:hover, &:focus {
            background-color: ${props.theme.palette.brand.light};
          }

          &:active {
            background-color: ${props.theme.palette.brand.dark};
          }
        `
    }}
  `

  const { children, ...other } = props

  return <StyledButton {...other}>{children}</StyledButton>
}
