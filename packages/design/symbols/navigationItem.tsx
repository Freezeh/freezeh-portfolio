import * as React from 'react'
import styled from 'styled-components'

interface NavigationItemProps {
  text: string
}

export const NavigationItem = (props: NavigationItemProps) => {
  const { text } = props

  const NavItem = styled.a`
    ${props => props.theme.typography.subheading};
  `

  return <NavItem>{text}</NavItem>
}
