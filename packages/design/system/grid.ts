import {
  AlignContentProperty,
  AlignItemsProperty,
  DisplayProperty,
  FlexDirectionProperty,
  FlexFlowProperty,
  FlexWrapProperty,
  JustifyContentProperty,
} from 'csstype'
import { css } from 'styled-components'

import { theme } from '../theme'

export type ColNumProperty = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12
export type ColProperty = ColNumProperty | 'auto' | 'equal' | 'initial'

const DEFAULT_GRID_UNIT = '8px'
const DEFAULT_GRID_COLS = 12

/**
 *
 * @param gutter <nunmber> the gutter css property. Defaults to Core Gutter Unit.
 * @param display <DisplayProperty> the display type of the grid. Defaults to 'flex'
 * @param flexDirection <FlexDirectionProperty> the flex direction css property. Defaults to null
 * @param flexWrap <FlexWrapProperty> the flex wrap css property. Defaults to null
 * @param flexFlow <FlexFlowProperty> the flex flow css property. Defaults to null
 * @param justifyContent <JustifyContentProperty> the justify content css property. Defaults to null
 * @param alignItems <AlignItemsProperty> the align items css property. Defaults to null
 * @param alignContent <AlignContentProperty> the align content css property. Defaults to null
 */
const grid = (
  gutter: string | null = DEFAULT_GRID_UNIT,
  display: DisplayProperty | null = 'flex',
  flexDirection: FlexDirectionProperty | null = null,
  flexWrap: FlexWrapProperty | null = null,
  flexFlow: FlexFlowProperty | null = null,
  justifyContent: JustifyContentProperty | null = null,
  alignItems: AlignItemsProperty | null = null,
  alignContent: AlignContentProperty | null = null,
) => css`
  display: ${display};
  flex-direction: ${flexDirection};
  flex-wrap: ${flexWrap};
  flex-flow: ${flexFlow};
  justify-content: ${justifyContent};
  align-items: ${alignItems};
  align-content: ${alignContent};
  ${grid_spacing('padding', gutter, gutter)}
`

/**
 *
 * @param col <ColProperty> Columns for class to span. Defaults to 'auto'.
 * @param gridColumns <ColNumProperty> Number of columns that the parent contain covers. Defaults to 12.
 * @param gutter <string?> Left and right gutter width of class. Defaults to coreGridColumns.
 * @param alignSelf <string?> Align-self value for class. Optional.
 */
const grid_column = (
  col: ColProperty = 'auto',
  gridColumns: ColNumProperty = DEFAULT_GRID_COLS,
  gutter: '0' | '8px' = DEFAULT_GRID_UNIT,
  alignSelf?: string,
) => {
  const flexGrow = col === 'auto' || col === 'equal' ? 1 : 0
  const flexShrink =
    col === 'auto' || col === 'equal' || col === 'initial' ? 1 : 0

  const cssObj = css`
    box-sizing: border-box;
    align-self: ${alignSelf};
    ${grid_spacing('margin', gutter, gutter)};
    flex-grow: ${flexGrow};
    flex-shrink: ${flexShrink};
    ${flexBasis(col, gridColumns, gutter)}
    ${col === 'auto' &&
      css`
        display: flex;
        width: auto;
        max-width: 100%;
      `}
  `

  return cssObj
}

const flexBasis = (
  col: ColProperty,
  gridColumns: number,
  gutter: string | null | undefined,
) => {
  let flexBasis

  if (typeof col === 'number') {
    flexBasis = `${(col / gridColumns) * 100}%`
    let deductGutter = false

    if (gutter) {
      flexBasis = `calc(${flexBasis} - ${gutter})`
    } else if (gutter) {
      deductGutter = true
    }

    if (deductGutter === true) {
      return css`
        flex-basis: calc(${flexBasis} - (${gutter} * 2));
        ${theme.mq[0]} {
          flex-basis: calc(${flexBasis} - (${gutter} * 4));
        }
      `
    }
  } else if (col === 'auto' || col === 'initial') {
    flexBasis = 'auto'
  } else if (col === 'equal') {
    flexBasis = 0
  }

  return css`
    flex-basis: ${flexBasis};
  `
}

const grid_spacing = (
  type: 'margin' | 'padding',
  left: string | null = null,
  right: string | null = null,
) => {
  return css`
    ${type}-left: ${left};
    ${type}-right: ${right};
    ${theme.mq[0]} {
      ${type}-left: ${left ? `calc(${left} * 2)` : null};
      ${type}-right: ${right ? `calc(${right} * 2)` : null};
    }
  `
}

export { grid, grid_column }
