const withTM = require('next-transpile-modules')
const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin')

module.exports = withTM({
  transpileModules: ['@freezeh'],
  webpack: config => {
    config.resolve.alias = {
      ...(config.resolve.alias || {}),
    }

    config.plugins.push(
      new SWPrecacheWebpackPlugin({
        minify: true,
        verbose: true,
        staticFileGlobsIgnorePatterns: [/\.next\//],
        runtimeCaching: [
          {
            handler: 'networkFirst',
            urlPattern: /^https?.*/,
          },
          {
            handler: 'networkFirst',
            urlPattern: /\/_next\/.*/,
          },
        ],
      }),
    )

    config.crossOrigin = 'anonymous'

    return config
  },
})
