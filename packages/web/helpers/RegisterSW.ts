const registerSW = async () => {
  if ('serviceWorker' in navigator) {
    try {
      const registration = await navigator.serviceWorker.getRegistration('/')
      if (!registration) {
        await navigator.serviceWorker.register('/sw.js', {
          scope: '/',
        })
      }
    } catch (e) {
      console.warn(`Registration failed: ${e}`)
    }
  }
}

export default registerSW
