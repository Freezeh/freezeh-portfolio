import { useEffect } from 'react'
import registerSW from '../helpers/RegisterSW'

const ServiceWorker = () => {
  useEffect(() => {
    registerSW()
  }, [])

  return null
}

export default ServiceWorker
