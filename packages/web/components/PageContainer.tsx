import * as React from 'react'
import { styled } from '@freezeh/design/theme'

interface PageProps {
  children?: React.ReactNode
}

export const PageContainer = (props: PageProps) => {
  const { children } = props

  const StyledContainer = styled.div`
    max-width: ${props => props.theme.siteWidth};
    margin-left: auto;
    margin-right: auto;
  `
  return <StyledContainer>{children}</StyledContainer>
}
