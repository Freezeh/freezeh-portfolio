import * as React from 'react'
import { Navigation } from '../Menu'
import { styled } from '@freezeh/design/theme'
import { grid } from '@freezeh/design'

export const Header = () => {
  const StyledHeader = styled.header`
    ${grid()}
    padding-bottom: 48px;
  `

  const StyledLogo = styled.div`
    height: 50px;
    width: 50px;
    background: red;
  `

  return (
    <StyledHeader>
      <StyledLogo />
      <Navigation />
    </StyledHeader>
  )
}
