import { styled } from '@freezeh/design/theme'
import Link from 'next/link'
import { SingletonRouter, withRouter } from 'next/router'
import * as React from 'react'

interface NavigationItemProps {
  text: string
  href: string
  router: SingletonRouter
}

export const NavigationItem = withRouter((props: NavigationItemProps) => {
  const { text, href, router } = props

  const NavItem = styled.a`
    cursor: pointer;
    text-decoration: none;
    color: ${props => props.theme.palette.greys.darker};
    ${props => props.theme.typography.navigation as any};
    padding: 8px 0;

    &:hover,
    &:focus {
      border-bottom: 2px solid ${props => props.theme.palette.brand.light};
      color: ${props => props.theme.palette.brand.light};
    }

    &:active {
      border-bottom: 2px solid ${props => props.theme.palette.brand.dark};
      color: ${props => props.theme.palette.brand.dark};
    }

    &.active {
      border-bottom: 4px solid ${props => props.theme.palette.brand.light};
    }
  `

  return (
    <Link href={href} as="">
      <NavItem
        className={
          `/${router.pathname.split('/')[1]}` === href ? `active` : undefined
        }
        href={href}
      >
        {text}
      </NavItem>
    </Link>
  )
})
