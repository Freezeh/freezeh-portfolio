import * as React from 'react'
import { styled } from '@freezeh/design/theme'
import { NavigationItem } from './NavigationItem'
import { grid_column } from '@freezeh/design'

export const Navigation = () => {
  const StyledNavigation = styled.div`
    ${grid_column('auto', undefined, '8px', 'center')}
    justify-content: center;
    a {
      margin-right: 8px;
    }
  `

  return (
    <StyledNavigation>
      <NavigationItem text="Home" href="/" />
      <NavigationItem text="About" href="/about" />
      <NavigationItem text="Projects" href="/projects" />
      <NavigationItem text="Contact" href="/contact" />
    </StyledNavigation>
  )
}
