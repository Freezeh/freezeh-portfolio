import { theme } from '@freezeh/design/theme'
import Document, {
  DocumentContext,
  Head,
  Main,
  NextScript,
} from 'next/document'
import * as React from 'react'
import { ServerStyleSheet } from 'styled-components'

import ServiceWorker from '../components/ServiceWorker'

export default class Doc extends Document<any> {
  static async getInitialProps(ctx: DocumentContext) {
    const sheet = new ServerStyleSheet()
    const originalRenderPage = ctx.renderPage

    try {
      ctx.renderPage = () =>
        originalRenderPage({
          enhanceApp: (App: any) => (props: any) =>
            sheet.collectStyles(<App {...props} />),
        })

      const initialProps = await Document.getInitialProps(ctx)
      const result: any = {
        ...initialProps,
        styles: (
          <>
            {initialProps.styles}
            {sheet.getStyleElement()}
          </>
        ),
      }
      return result
    } finally {
      sheet.seal()
    }
  }

  render() {
    return (
      <html lang="en">
        <Head>
          {this.props.styleTags}
          <meta charSet="utf-8" />
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
          />
          <meta name="theme-color" content="#605B55" />
          <link rel="icon" href="/static/icons/i-64.png" />
          <link rel="manifest" href="/static/manifest.json" />
          <link href={theme.typography.fontUrl} rel="stylesheet" />
        </Head>
        <body className="body">
          <Main />
          <NextScript />
          <ServiceWorker />
        </body>
      </html>
    )
  }
}
