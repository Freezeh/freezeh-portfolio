import { Button } from '@freezeh/design/symbols'
import React from 'react'

const Home = () => {
  return (
    <>
      <Button emphasis="primary">Test</Button>
    </>
  )
}

export default Home
