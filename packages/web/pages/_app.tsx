import { theme, ThemeProvider } from '@freezeh/design/theme'
import App, { Container } from 'next/app'
import React from 'react'

import { Header, PageContainer } from '../components'

export default class MyApp extends App {
  static async getInitialProps(props: any) {
    let pageProps = {}

    if (props.Component.getInitialProps) {
      pageProps = await props.Component.getInitialProps(props.ctx)
    }

    return { pageProps }
  }

  render() {
    const { Component, pageProps } = this.props
    return (
      <Container>
        <ThemeProvider theme={theme}>
          <PageContainer>
            <Header />
            <Component {...pageProps} />
          </PageContainer>
        </ThemeProvider>
      </Container>
    )
  }
}
